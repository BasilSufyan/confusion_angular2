import { Injectable } from '@angular/core';
import { Dish } from '../app/Shared/dish';
import { DISHES } from '../app/Shared/dishes';

@Injectable()
export class DishServiceService {

  constructor() { }

  getDishes():Dish[]{
    return DISHES;
  }
}
