import { Injectable } from '@angular/core';
import { Dish } from '../Shared/dish';
import { DISHES } from '../Shared/dishes';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DishServiceService {

  constructor() { }

  // getDishes():Dish[]{
  //   return DISHES;
  // }

  // getDish(id:number):Dish{
  //   return DISHES.filter((dish) => id==dish.id)[0]; 
  // }

  // getFeaturedDish(): Dish {
  //   return DISHES.filter((dish) => dish.featured)[0];
  // }

  getDishes(): Observable<Dish[]> {
    return Observable.of(DISHES).delay(2000);
  }

  getDish(id: number): Observable<Dish> {  
    return Observable.of(DISHES.filter((dish) => id==dish.id)[0]).delay(2000);
  }

  getFeaturedDish(): Observable<Dish> {
    return Observable.of(DISHES.filter((dish) => dish.featured)[0]).delay(2000);
  }

  getDishIds(): Observable<number[]>{
    return Observable.of(DISHES.map(dish => dish.id)).delay(2000);
  }
}
