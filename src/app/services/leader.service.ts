import { Injectable } from '@angular/core';
import { Leader } from '../Shared/leader';
import { LEADERS } from '../Shared/leaders';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';


@Injectable()
export class LeaderService {

//  getLeader(id:number):Leader{
//    return LEADERS.filter((Leader)=> id==Leader.id)[0];
//  }

//  getLeaders():Leader[]{
//    return LEADERS;
//  }

//  getFeaturedLeader():Leader{
//    return LEADERS.filter((Leader)=> Leader.featured)[0];
//  }

//  getLeader(id:number):Promise<Leader>{
//   return new Promise(resolve => {
//      setTimeout(()=>resolve(LEADERS.filter((Leader)=> id==Leader.id)[0]),2000);
//   });
//   }

//   getLeaders():Promise<Leader[]>{
//   return new Promise(resolve => {
//     setTimeout(()=>resolve(LEADERS),2000);
//   });
//   }

//   getFeaturedLeader():Promise<Leader>{
//   return new Promise(resolve => {
//     setTimeout(()=>resolve(LEADERS.filter((Leader)=> Leader.featured)[0]),2000); 
//   });  
//   }

    getLeader(id:number):Observable<Leader>{
    return Observable.of(LEADERS.filter((Leader)=> id==Leader.id)[0]).delay(2000);
    }
  
    getLeaders():Observable<Leader[]>{
    return Observable.of(LEADERS).delay(2000);
    }
  
    getFeaturedLeader():Observable<Leader>{
    return Observable.of(LEADERS.filter((Leader)=> Leader.featured)[0]).delay(2000);
    }
  constructor() { }

}
