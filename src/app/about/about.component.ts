import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Leader } from '../Shared/leader';
import { LEADERS }from '../Shared/leaders';
import { LeaderService } from '../services/leader.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutComponent implements OnInit {

  leaders:Leader[];

  constructor(private leaderSvc:LeaderService) { }


  ngOnInit() {
    //this.leaders = this.leaderSvc.getLeaders();
    this.leaderSvc.getLeaders().subscribe(leaders => this.leaders = leaders);
  }

}
