import {Component, OnInit,  Input} from '@angular/core';
import { Dish } from '../Shared/dish';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishServiceService } from '../services/dish-service.service';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-dishdetail',
    templateUrl: './dishdetail.component.html',    
    
  })
  
  export class dishdetailComponent implements OnInit {     
      
    dish : Dish;  
    dishIds: number[];
    prev: number;
    next: number; 
    
    constructor(private dishService:DishServiceService,private route:ActivatedRoute ,private location:Location) {}  
    
  
    ngOnInit() {
      this.dishService.getDishIds().subscribe(dishIds => this.dishIds=dishIds);

      let id= +this.route.params
      .switchMap((params:Params) => this.dishService.getDish(+params['id']))
      .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); });
    
      // this.dish = this.dishService.getDish(id);
      //this.dishService.getDish(id).subscribe(dish => this.dish = dish);      
    }

    setPrevNext(dishId: number) {
      let index = this.dishIds.indexOf(dishId);
      this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
      this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
    }
  
    goBack():void{
      this.location.back();
    }
  }
  