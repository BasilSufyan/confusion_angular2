import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DishServiceService } from '../services/dish-service.service';
import { Dish } from '../Shared/dish';
import { Promotion } from'../Shared/promotion';
import { PromotionService } from '../services/promotion.service';
import { LeaderService } from '../services/leader.service';
import { Leader} from '../Shared/leader';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  dish:Dish;
  promotion:Promotion;
  leader:Leader;


  constructor(private dishService:DishServiceService, private promotionService:PromotionService,
     private leaderService:LeaderService ) { }

  ngOnInit() {
    //this.dish = this.dishService.getFeaturedDish();
    this.dishService.getFeaturedDish().subscribe(dish => this.dish = dish);
    //this.promotion = this.promotionService.getFeaturedPromotion();
    this.promotionService.getFeaturedPromotion().subscribe
    (promotion => this.promotion = promotion);
    //this.leader = this.leaderService.getFeaturedLeader();
    this.leaderService.getFeaturedLeader().subscribe(leader => this.leader = leader);
  }

}
