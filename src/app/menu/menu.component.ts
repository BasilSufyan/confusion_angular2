import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Dish } from '../Shared/dish';
import { DishServiceService } from'../services/dish-service.service'; 

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit { 
  dishes: Dish[] ;
  selectedDish: Dish;
  
  constructor(private dishService:DishServiceService) {}

  ngOnInit() {
    //this.dishes = this.dishService.getDishes();
    this.dishService.getDishes().subscribe(dishes => this.dishes = dishes);
  }

  onSelect(dish: Dish) {
    this.selectedDish = dish;
  }
  

}
